terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone = "ru-central1-b"
}

resource "yandex_compute_instance" "tf-vm-1" {
  name        = "terraform-vm-1"
  platform_id = "standard-v1" #  "Intel Broadwell"
  zone        = "ru-central1-b"

  resources {
    cores  = 2
    memory = 1
    core_fraction = 5 # Гарантированная доля vCPU
}

  boot_disk {
    initialize_params {
      image_id = "fd8gnpl76tcrdv0qsfko"
      size = 10
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.terraform-subnet1.id}"
  }

  metadata = {
#    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    user-data = "${file("./meta")}"
  }

  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_vpc_network" "terraform-network1" {}

resource "yandex_vpc_subnet" "terraform-subnet1" {
  zone           = "ru-central1-b"
  network_id     = "${yandex_vpc_network.terraform-network1.id}"
  v4_cidr_blocks = ["10.5.0.0/24"]
}

resource "yandex_vpc_address" "tf-external-ip" {
  name = "tf-external-ip"
  folder_id = "b1g02p44sca5uv3mjf3p"

  external_ipv4_address {
    zone_id = "ru-central1-b"
  }
}
